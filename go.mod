module github.com/eventials/goevents

go 1.12

require (
	github.com/aws/aws-sdk-go v1.18.0
	github.com/sirupsen/logrus v1.4.0
	github.com/streadway/amqp v0.0.0-20190312002841-61ee40d2027b
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190311183353-d8887717615a // indirect
)
